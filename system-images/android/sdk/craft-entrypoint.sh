#!/bin/bash
# SPDX-FileCopyrightText: none
# SPDX-License-Identifier: CC0-1.0

# load the Craft environment if Craft has been set up
if [ -f ~/CraftRoot/craft/craftenv.sh ] && [ -z "$CRAFT_PYTHON_DIR" ]; then
    pushd `pwd` > /dev/null
    source ~/CraftRoot/craft/craftenv.sh
    popd > /dev/null
fi

exec "$@"
