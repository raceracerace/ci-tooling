variables:
  PROJECT: $CI_PROJECT_NAME
  CI_TOOLING: /build/ci-tooling

default:
  before_script:
    - git clone --depth=1 https://invent.kde.org/sysadmin/ci-tooling.git $CI_TOOLING
    - git clone --depth=1 https://invent.kde.org/sysadmin/repo-metadata.git $CI_TOOLING/repo-metadata
    - git clone --depth=1 https://invent.kde.org/sysadmin/kde-build-metadata.git $CI_TOOLING/kde-build-metadata
    - git clone --depth=1 https://invent.kde.org/frameworks/kapidox.git $CI_TOOLING/kapidox
    - git clone --depth=1 https://invent.kde.org/sdk/kde-dev-scripts.git $CI_TOOLING/kde-dev-scripts

.linux:
  stage: build
  variables:
    INSTALL_PREFIX: /home/jenkins/install-prefix
  script:
    - "python3 -u $CI_TOOLING/helpers/prepare-dependencies.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --environment production --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/configure-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/compile-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --usingInstall $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/install-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/run-tests.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --usingInstall $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/print-lcov-results.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM"
  coverage: '/^TOTAL.*\s+(\d+\%)$/'
  artifacts:
    reports:
      junit: JUnitTestResults.xml

.android:
  stage: build
  variables:
    INSTALL_PREFIX: /home/user/install-prefix
  script:
    - "python3 -u $CI_TOOLING/helpers/prepare-dependencies.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --environment production --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/configure-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/compile-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --usingInstall $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/install-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --installTo $INSTALL_PREFIX"

.freebsd:
  stage: build
  variables:
    INSTALL_PREFIX: /home/jenkins/install-prefix
    CI_TOOLING: /home/jenkins/ci-tooling
  tags:
    - freebsd
  script:
    - "python3 -u $CI_TOOLING/helpers/prepare-dependencies.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --environment production --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/configure-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/compile-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --usingInstall $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/install-build.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --installTo $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/run-tests.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM --usingInstall $INSTALL_PREFIX"
    - "python3 -u $CI_TOOLING/helpers/print-lcov-results.py --product $PRODUCT --project $PROJECT --branchGroup $BRANCH_GROUP --platform $PLATFORM"
  coverage: '/^TOTAL.*\s+(\d+\%)$/'
  artifacts:
    reports:
      junit: JUnitTestResults.xml
  after_script:
    - "rm -rf $CI_TOOLING"
    - "rm -rf $INSTALL_PREFIX"

.windows:
  stage: build
  variables:
    CI_TOOLING: C:/CI/ci-tooling
    INSTALL_PREFIX: C:/CI/Software Installs
    CRAFT_ROOT: C:/Craft/CI-Qt513/windows-msvc2017_64-cl-debug/
  tags:
    - windows
  before_script:
    - if exist "%CI_TOOLING%" rmdir /s /q "%CI_TOOLING%"
    - git clone --depth=1 https://invent.kde.org/sysadmin/ci-tooling.git $CI_TOOLING
    - git clone --depth=1 https://invent.kde.org/sysadmin/repo-metadata.git $CI_TOOLING/repo-metadata
    - git clone --depth=1 https://invent.kde.org/sysadmin/kde-build-metadata.git $CI_TOOLING/kde-build-metadata
    - git clone --depth=1 https://invent.kde.org/frameworks/kapidox.git $CI_TOOLING/kapidox
    - git clone --depth=1 https://invent.kde.org/sdk/kde-dev-scripts.git $CI_TOOLING/kde-dev-scripts
    - if exist "%INSTALL_PREFIX%/%PROJECT%" rmdir /s /q "%INSTALL_PREFIX%/%PROJECT%"
  script:
    - call "C:/Program Files (x86)/Microsoft Visual Studio/2017/Professional/VC/Auxiliary/Build/vcvars64.bat"
    - python -u %CI_TOOLING%/helpers/prepare-dependencies.py --product %PRODUCT% --project %PROJECT% --branchGroup %BRANCH_GROUP% --environment production --platform %CI_PLATFORM% --installTo "%INSTALL_PREFIX%/%PROJECT%"
    - python -u %CI_TOOLING%/helpers/configure-build.py --product %PRODUCT% --project %PROJECT% --branchGroup %BRANCH_GROUP% --platform %CI_PLATFORM% --installTo "%INSTALL_PREFIX%/%PROJECT%"
    - python -u %CI_TOOLING%/helpers/compile-build.py --product %PRODUCT% --project %PROJECT% --branchGroup %BRANCH_GROUP% --platform %CI_PLATFORM% --usingInstall "%INSTALL_PREFIX%/%PROJECT%"
    - python -u %CI_TOOLING%/helpers/install-build.py --product %PRODUCT% --project %PROJECT% --branchGroup %BRANCH_GROUP% --platform %CI_PLATFORM% --installTo "%INSTALL_PREFIX%/%PROJECT%"
    - python -u %CI_TOOLING%/helpers/run-tests.py --product %PRODUCT% --project %PROJECT% --branchGroup %BRANCH_GROUP% --platform %CI_PLATFORM% --usingInstall "%INSTALL_PREFIX%/%PROJECT%"
    - python -u %CI_TOOLING%/helpers/print-lcov-results.py --product %PRODUCT% --project %PROJECT% --branchGroup %BRANCH_GROUP% --platform %CI_PLATFORM%
  artifacts:
    reports:
      junit: JUnitTestResults.xml
  after_script:
    - rmdir /s /q "%CI_TOOLING%"
